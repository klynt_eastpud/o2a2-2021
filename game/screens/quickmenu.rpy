
init offset = -1

## Quick Menu screen ###########################################################
##
## The quick menu is displayed in-game to provide easy access to the out-of-game
## menus.

screen quick_menu():

    ## Ensure this appears on top of other screens.
    zorder 100

    if quick_menu:

        hbox:
            xpos 0
            yalign 1.0
            offset (10, -10)
            spacing 10
            style_prefix "quick"

            textbutton _("Save") action ShowMenu('save')
            textbutton _("Load") action ShowMenu('load')
            textbutton _("Skip") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("History") action ShowMenu('history')
            textbutton _("Quick save") action QuickSave()
            textbutton _("Preferences") action ShowMenu('preferences')


## This code ensures that the quick_menu screen is displayed in-game, whenever
## the player has not explicitly hidden the interface.
init python:
    config.overlay_screens.append("quick_menu")

default quick_menu = True

style quick_button is default
style quick_button_text is button_text

style quick_button:
    xpadding 10
    ypadding 10
    properties gui.button_properties("quick_button")

style quick_button_text:
    size 20
    properties gui.button_text_properties("quick_button")
