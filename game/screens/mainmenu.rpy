
init offset = -1

## Main Menu screen ############################################################
##
## Used to display the main menu when Ren'Py starts.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

define menu_items = [
    (_("Start"), Start()),
    (_("Load"), ShowMenu("load")),
    (_("Preferences"), ShowMenu("preferences")),
    (_("About"), ShowMenu("about")),
    (_("Help"), ShowMenu("help")),
    (_("Quit"), Quit(confirm=not main_menu))
]

# this code was used to generate the cover art shown on itch
# image rhodes_repeat:
#     "rhodes"
#     alpha 0.5
#     xpos -1060
#     ypos -500
#     zoom 0.8

define start_offset = -70

screen main_menu():

    ## This ensures that any other menu screen is replaced.
    tag menu

    style_prefix "main_menu"

    add "lightgrey"

    # this code was used to generate the cover art shown on itch
    # for i in range(0, 30):
    #     if i == 0:
    #         pass
    #     else:
    #         add "rhodes_repeat":
    #             xoffset (50 / (i * (i * 0.0004))) - 10

    window:
        vbox:
            spacing 20
            xalign 0.5
            vbox:
                xalign 0.5
                xsize 290
                text "[config.name!t]":
                    style "main_menu_title"

                text "version [config.version]":
                    style "main_menu_version"

            for i, (title, ac) in enumerate(menu_items):
                button action ac:
                    xsize 290
                    xoffset start_offset + (i * 30)
                    background "#fff"
                    hover_background "#555"
                    text title:
                        xalign 0.5
                        color "#555"
                        hover_color "#fff"

    add "lights"


style main_menu_frame is empty
style main_menu_vbox is vbox
style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text

style some_bullshit:
    background "#fff"
    hover_background "#555"

style main_menu_window:
    xalign 0.5
    yalign 0.5

style main_menu_button:
    xpadding 30

style main_menu_text:
    hover_color "#fff"
    color "#000"

style main_menu_title:
    properties gui.text_properties("title")

style main_menu_version:
    xalign 1.0
    properties gui.text_properties("version")
