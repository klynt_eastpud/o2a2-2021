
init offset = -1

## Say screen ##################################################################
##
## The say screen is used to display dialogue to the player. It takes two
## parameters, who and what, which are the name of the speaking character and
## the text to be displayed, respectively. (The who parameter can be None if no
## name is given.)
##
## This screen must create a text displayable with id "what", as Ren'Py uses
## this to manage text display. It can also create displayables with id "who"
## and id "window" to apply style properties.
##
## https://www.renpy.org/doc/html/screen_special.html#say

define use_alt_saybox = False

screen say(who, what):
    style_prefix "say"
    frame:
        offset (10, 10)
        xpos 30
        xsize 400
        xpadding 30
        ypadding 30
        yalign gui.textbox_yalign
        ysize gui.textbox_height
        if not use_alt_saybox:
            background Solid("#bbb")
        else:
            background Solid("#222")
    frame:
        id "window"
        if not use_alt_saybox:
            background Solid("#eee")
        else:
            background Solid("#555")

        if who is not None:

            window:
                id "namebox"
                style "namebox"
                if not use_alt_saybox:
                    text who id "who"
                else:
                    background Solid("#222")
                    text who id "who" color "#eee"

        if not use_alt_saybox:
            text what id "what"
        else:
            text what id "what" color "#eee"

    ## If there's a side image, display it above the text. Do not display on the
    ## phone variant - there's no room.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0


## Make the namebox available for styling through the Character object.
init python:
    config.character_id_prefixes.append('namebox')

style window is default
style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style namebox is default
style namebox_label is say_label


style window:
    xpos 30
    xsize 400
    xpadding 30
    ypadding 30
    yalign gui.textbox_yalign
    ysize gui.textbox_height

style namebox:
    xpos gui.name_xpos
    xanchor gui.name_xalign
    xsize gui.namebox_width
    ypos gui.name_ypos
    ysize gui.namebox_height
    background Solid("#bbb")
    padding gui.namebox_borders.padding
    # background Frame("gui/namebox.png", gui.namebox_borders, tile=gui.namebox_tile, xalign=gui.name_xalign)

style say_label:
    properties gui.text_properties("name", accent=True)
    xalign gui.name_xalign
    yalign 0.5

style say_dialogue:
    properties gui.text_properties("dialogue")

    xpos gui.dialogue_xpos
    xsize gui.dialogue_width
    ypos gui.dialogue_ypos
