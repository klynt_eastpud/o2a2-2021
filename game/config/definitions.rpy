
# Declare characters used by this game. The color argument colorizes the
# name of the character.
init -1:

    image ctc:
        "ctc_arrow"
        ypos 20
        zoom 0.5
        xpos 10
        ease 0.25 xpos 20
        ease 0.25 xpos 10
        0.5
        repeat

    # CHARACTERS
    $ default = Character(None,
        what_prefix=u"\u201C",
        what_suffix=u"\u201D",
        ctc="ctc"
        )

    $ You = Character("You", kind=default)
    $ Rhodes = Character("Rhodes", kind=default)
    $ Girl = Character("Girl", kind=default)
    $ N = Character(None, ctc="ctc")
    $ NVL = Character(None, kind=nvl)

    ## EFFECTS
    $ flashwhite = Fade(0.1, 0.0, 0.4, color='#fff')
    $ flashred = Fade(0.1, 0.0, 0.4, color='#ff0000')
    $ flashblack = Fade(0.1, 0.0, 0.4, color='#000')

    # COLOURS
    image white = Solid("#fff")
    image white_overlay = Solid("#fff")
    image black = Solid("#000")
    image black_overlay = Solid("#000")
    image grey = Solid("#ccc")
    image lightgrey = Solid("#eee")
    image blue = Solid("#013")
    image red = Solid("#f00")
    define squaresize = 100
    image blacksquare = Solid("#000", xsize=squaresize, ysize=squaresize)

    # TRANSITIONS
    define dissolve = Dissolve(0.25)
    define wiperight = CropMove(0.25, "wiperight")
    define slowwipeup = CropMove(1.0, "wipeup")
    define long_dissolve = Dissolve(0.5)
    define fade = Fade(0.5, 0.25, 0.5)
    define long_fade = Fade(1.0, 0.5, 1.0)
    define short_pixellate = Pixellate(0.25, 5)
    define long_pixellate = Pixellate(2.0, 10)

    # TRANSFORMS
    transform zoom_face:
        xalign 0.35
        yalign 0.08

    transform close:
        xalign 0.1
        yalign 0.08
        zoom 0.7

    transform close_right:
        xalign -0.15
        yalign 0.08
        zoom 0.7

    transform middle:
        xalign 0.75
        yalign 0.08
        zoom 0.2

    transform middle_pan_up:
        xalign 0.75 zoom 0.2 yalign 1.5
        ease 5.0 yalign 0.08

    transform randomise_colours(params):
        matrixcolor Matrix(params)

    transform blurry:
        blur 50.0

    transform very_blurry:
        blur 100.0

    define distance = -5

    transform middle_laughing:
        xalign 0.75
        yalign 0.08
        zoom 0.2
        yoffset 0
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        pause 1.5
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        pause 1.0
        repeat

    transform middle_bounce:
        xalign 0.75
        yalign 0.08
        zoom 0.2
        yoffset 0
        linear 0.05 yoffset -5
        linear 0.05 yoffset 0

    transform middle_jump:
        xalign 0.75
        yalign 0.08
        zoom 0.2
        yoffset 0
        linear 0.05 yoffset -20
        linear 0.05 yoffset 0

    transform zoom_face_laughing:
        xalign 0.35
        yalign 0.08
        yoffset 0
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        pause 1.5
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        linear 0.1 yoffset distance
        linear 0.1 yoffset 0
        pause 1.0
        repeat

    image lift_going_up:
        "white_overlay"
        ypos global_screen_height * 2
        linear 1.5 ypos global_screen_height * -1
        pause 0.5
        repeat

    image droplet:
        "circle"
        rotate 180
        xsize 20
        ysize 50
        alpha 0.5
        matrixcolor ColorizeMatrix("#fff", "#fff")

    image particle:
        "circle"
        zoom 2.0
        additive True
        alpha 0.5
        choice:
            matrixcolor ColorizeMatrix("#055", "#055")
        choice:
            matrixcolor ColorizeMatrix("#550", "#550")
        choice:
            matrixcolor ColorizeMatrix("#505", "#505")

    image sludge:
        "circle"
        additive True
        choice:
            matrixcolor ColorizeMatrix("#055", "#055")
        choice:
            matrixcolor ColorizeMatrix("#550", "#550")
        choice:
            matrixcolor ColorizeMatrix("#505", "#505")
        xsize 50
        ysize 300
        alpha 0.5

    image rain = SnowBlossom("droplet", count=10, border=50, xspeed=(20, 50), yspeed=(1000, 1500), start=0.5)
    image lights = SnowBlossom("particle", count=10, border=1000, xspeed=(-500, 500), yspeed=(-500, 500), start=0.5)
    image black_rain = SnowBlossom("sludge", count=10, border=50, xspeed=(20, 50), yspeed=(1000, 1500), start=0.5)

    image colour_flash:
        "blue"
        matrixcolor Matrix([1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0])
        pause 0.75
        choice:
            matrixcolor ColorizeMatrix("#055", "#055")
        choice:
            matrixcolor ColorizeMatrix("#550", "#550")
        choice:
            matrixcolor ColorizeMatrix("#505", "#505")
        pause 0.2
        repeat

    image colour_flash_light:
        "lightgrey"
        matrixcolor Matrix([1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0])
        pause 0.75
        choice:
            matrixcolor ColorizeMatrix("#eff", "#eff")
        choice:
            matrixcolor ColorizeMatrix("#ffe", "#ffe")
        choice:
            matrixcolor ColorizeMatrix("#fef", "#fef")
        pause 0.2
        repeat

    image blacksquaremove:
        additive True
        "blacksquare"
        choice:
            matrixcolor ColorizeMatrix("#055", "#055")
        choice:
            matrixcolor ColorizeMatrix("#550", "#550")
        choice:
            matrixcolor ColorizeMatrix("#505", "#505")
        parallel:
            choice:
                zoom 1.0
                easein 0.5 zoom 2.0
            choice:
                zoom 4.0
                easein 0.5 zoom 5.0
            easeout 0.5 zoom 1.0
            repeat
        parallel:
            rotate 0
            choice:
                linear 1.0 rotate 360
            choice:
                linear 0.5 rotate 360
            repeat

    image explode = SnowBlossom("blacksquaremove", count=20, border=200, xspeed=(-2000, 2000), yspeed=(-500, -1000), start=0.2)
    image explode2 = SnowBlossom("blacksquaremove", count=20, border=200, xspeed=(-2000, 2000), yspeed=(500, 1000), start=0.2)
