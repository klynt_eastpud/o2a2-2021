
layeredimage rhodes:

    group body:
        attribute body_normal default:
            "rhodes_base"
        attribute body_blush:
            "rhodes_base_blush"

    attribute empty_face:
        "rhodes_empty_face"

    group eyes:
        attribute e_normal default:
            "rhodes_eyes_normal"
        attribute e_closed_laugh:
            "rhodes_eyes_closed_laugh"
        attribute e_closed:
            "rhodes_eyes_closed"
        attribute e_smile:
            "rhodes_eyes_smile"
        attribute e_wide:
            "rhodes_eyes_wide"
        attribute e_wide_side:
            "rhodes_eyes_wide_side"

    group mouth:
        attribute m_normal default:
            "rhodes_mouth_normal"
        attribute m_grin:
            "rhodes_mouth_grin"
        attribute m_grimace:
            "rhodes_mouth_grimace"
        attribute m_laugh:
            "rhodes_mouth_laugh"
        attribute m_open:
            "rhodes_mouth_shout"
        attribute m_smile:
            "rhodes_mouth_smile"

    group hair:
        attribute hair default:
            "rhodes_hair"

    group eyebrows:
        attribute eb_normal default:
            "rhodes_eyebrows_normal"
        attribute eb_frown:
            "rhodes_eyebrows_frown"
        attribute eb_oneup:
            "rhodes_eyebrows_oneup"
        attribute eb_raised:
            "rhodes_eyebrows_raised"
        attribute eb_sad:
            "rhodes_eyebrows_worried"

layeredimage rhodes_flip:
    xzoom -1.0

    group body:
        attribute body_normal default:
            "rhodes_base"
        attribute body_blush:
            "rhodes_base_blush"

    attribute empty_face:
        "rhodes_empty_face"

    group eyes:
        attribute e_normal default:
            "rhodes_eyes_normal"
        attribute e_closed_laugh:
            "rhodes_eyes_closed_laugh"
        attribute e_closed:
            "rhodes_eyes_closed"
        attribute e_smile:
            "rhodes_eyes_smile"
        attribute e_wide:
            "rhodes_eyes_wide"
        attribute e_wide_side:
            "rhodes_eyes_wide_side"

    group mouth:
        attribute m_normal default:
            "rhodes_mouth_normal"
        attribute m_grin:
            "rhodes_mouth_grin"
        attribute m_grimace:
            "rhodes_mouth_grimace"
        attribute m_laugh:
            "rhodes_mouth_laugh"
        attribute m_open:
            "rhodes_mouth_shout"
        attribute m_smile:
            "rhodes_mouth_smile"

    group hair:
        attribute hair default:
            "rhodes_hair"

    group eyebrows:
        attribute eb_normal default:
            "rhodes_eyebrows_normal"
        attribute eb_frown:
            "rhodes_eyebrows_frown"
        attribute eb_oneup:
            "rhodes_eyebrows_oneup"
        attribute eb_raised:
            "rhodes_eyebrows_raised"
        attribute eb_sad:
            "rhodes_eyebrows_worried"

image eyes_blink:
    "rhodes_eyes_normal"
    pause 2.0
    "rhodes_eyes_closed"
    pause 0.05
    "rhodes_eyes_normal"
    pause 2.0
    "rhodes_eyes_closed"
    pause 0.05
    "rhodes_eyes_normal"
    pause 0.1
    "rhodes_eyes_closed"
    pause 0.05
    repeat

layeredimage rhodes_transparent:

    always:
        "empty_base"

    group eyes:
        attribute e_normal default:
            "eyes_blink"

    group eyebrows:
        attribute eb_normal default:
            "rhodes_eyebrows_normal"

define amount = 4

define pos1 = (renpy.random.randint(-amount, amount), renpy.random.randint(-amount, amount))
define pos2 = (renpy.random.randint(-amount, amount), renpy.random.randint(-amount, amount))
define pos3 = (renpy.random.randint(-amount, amount), renpy.random.randint(-amount, amount))
define pos4 = (renpy.random.randint(-amount, amount), renpy.random.randint(-amount, amount))

transform shudder:
    pos (0, 0)
    pause 0.1
    pos pos1
    pause 0.05
    pos pos2
    pause 0.02
    pos (0, 0)
    pause 0.4
    pos pos3
    pause 0.05
    pos pos4
    pos (0, 0)
    pause 1.0
    repeat

layeredimage rhodes_empty:
    xzoom -1.0

    always:
        "rhodes_base"

    always:
        "rhodes_empty_face"

    group glitchy_overlay:
        attribute glitch:
            "rhodes_empty_face_glitch" at shudder

    group hair:
        attribute hair default:
            "rhodes_hair"

define far_colour = "#000000"
define mid_colour = "#111"

layeredimage rhodes_silhouette_far:

    always:
        "images/rhodes_base.png"
        matrixcolor ColorizeMatrix(far_colour, far_colour)

    always:
        "images/rhodes_hair.png"
        matrixcolor ColorizeMatrix(far_colour, far_colour)

layeredimage rhodes_silhouette_mid:

    always:
        "images/rhodes_base.png"
        matrixcolor ColorizeMatrix(mid_colour, mid_colour)

    always:
        "images/rhodes_hair.png"
        matrixcolor ColorizeMatrix(mid_colour, mid_colour)

define blur_amount_far = 60
define zoom_amount_far = 0.2

layeredimage crowd:
    yalign 0.0
    xalign 0.0
    crop (0,0,global_screen_width,global_screen_height)
    blur blur_amount_far

    group back:

        attribute small default:
            "rhodes_silhouette_far"
            xalign -0.1
            yalign 0.2
            zoom zoom_amount_far
            xzoom -1

        attribute small default:
            "rhodes_silhouette_mid"
            xalign 0.35
            yalign 0.1
            zoom zoom_amount_far

        attribute small default:
            "rhodes_silhouette_far"
            xalign 0.7
            yalign 0.0
            zoom zoom_amount_far
            xzoom -1

        attribute small default:
            "rhodes_silhouette_mid"
            xalign 1.1
            yalign 0.4
            zoom zoom_amount_far
            xzoom -1

define alpha_out = 0.1
define x_amount = 500
define y_amount = 500
define diagonal = 400

define north = (0, y_amount)
define northeast = (diagonal, diagonal)
define east = (x_amount, 0)
define southeast = (diagonal, -diagonal)
define south = (0, -y_amount)
define southwest = (-diagonal, -diagonal)
define west = (-x_amount, 0)
define northwest = (-diagonal, diagonal)

transform split1:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset north
    block:
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        repeat

transform split2:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset northeast
    block:
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        repeat

transform split3:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset east
    block:
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        repeat

transform split4:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset southeast
    block:
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        repeat

transform split5:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset south
    block:
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        repeat

transform split6:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset southwest
    block:
        linear 1.5 offset west
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        repeat

transform split7:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset west
    block:
        linear 1.5 offset northwest
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        repeat

transform split8:
    offset (0, 0) alpha 1.0 additive True
    easein 1.5 alpha alpha_out offset northwest
    block:
        linear 1.5 offset north
        linear 1.5 offset northeast
        linear 1.5 offset east
        linear 1.5 offset southeast
        linear 1.5 offset south
        linear 1.5 offset southwest
        linear 1.5 offset west
        linear 1.5 offset northwest
        repeat

layeredimage rhodes_split_parts:
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split1
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split2
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split3
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split4
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split5
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split6
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split7
    always:
        "rhodes body_blush e_wide eb_raised m_smile" at split8

image rhodes_split:
    "rhodes_split_parts"
    xalign 0.35
    yalign 0.08
