
label start:

    $ points = 0

    with Pause(1.0)
    scene white with long_dissolve

    show rhodes eb_oneup e_wide m_open at zoom_face with vpunch

    Girl "Hey are you listening?"
    with long_pixellate
    # N "You blink a few times, snapping out of your daze."
    You "Uh, yeah. {w}Yeah, sorry."
    show rhodes m_normal at zoom_face with dissolve
    You "What were you saying?"
    show rhodes eb_raised e_closed_laugh m_laugh at zoom_face_laughing with dissolve
    N "She laughs and her whole face lights up like a sunbeam."
    Girl "You fucking ditz!"
    show white_overlay:
        alpha 0.0
        linear 5.0 alpha 0.5
    N "You remember now."
    N "Rhodes."
    N "Loud, {w}abrasive, {w}totally gorgeous."
    N "The kind of girl you always wished {i}you{/i} were."
    N "You bumped into her on the way home from work."
    N "And then to your complete surprise, she looked at you with a kind of awestruck expression that no one's ever shown you before."
    N "Someone like {i}her{/i}."
    N "And she noticed {i}you{/i}."
    N "Then she dragged you away to get coffee."
    # N "Yeah, that's what happened."
    # N "And here you are."

    hide white_overlay
    show rhodes eb_raised e_closed_laugh m_grin at middle_bounce
    with dissolve

    Rhodes "Sooo..."
    Rhodes "My place. {w}Tonight."
    show rhodes eb_raised e_wide m_grin with dissolve
    Rhodes "Can you come?"
    with short_pixellate
    with short_pixellate
    N "You blink a few times."
    # N "You blink a few more times."
    # N "That's right. She was telling you about a party or something."
    # N "Was it a party?"  # maybe
    # N "Wait a minute..."
    You "But we're still in lockdown! {w}...Aren't we?"
    with short_pixellate
    show rhodes eb_oneup e_wide m_normal with dissolve
    N "She tilts her head to the side, confused."
    with short_pixellate
    with short_pixellate
    Rhodes "Lockdown? What are you talking about?"
    You "Uh. {w}You know. {w}The pandemic?"
    show rhodes eb_raised e_wide m_open at middle_jump
    Rhodes "A pandemic?"
    show rhodes eb_raised e_closed_laugh m_laugh at middle_laughing
    N "Then she just starts {i}laughing{/i} and it's as if something clicks in your head."
    with short_pixellate
    with short_pixellate
    N "Yeah, what the hell? A pandemic?"
    # N "What's wrong with your head today?"
    N "What sort of apocalyptic delusions are you having?"
    You "Uhh. {w}Sorry."
    show rhodes eb_sad e_wide m_open at middle
    # You "My head's feelin' kinda funny."
    Rhodes "You alright?"
    N "She glances down at your latte that you've barely touched."
    Rhodes "You should drink your coffee. Maybe it'll help."
    N "You take a sip of it and it feels like reality falls back into place - right where it should be again."
    N "That was weird."
    show rhodes eb_raised e_closed_laugh m_laugh at middle_bounce
    Rhodes "So! The paaaarty!"
    You "I dunno..."
    You "Work is really busy right now with all this brexit stuff."
    with short_pixellate
    show rhodes eb_oneup e_wide m_open with dissolve
    Rhodes "Brexit?"
    show rhodes eb_sad e_closed_laugh m_laugh at middle_laughing
    N "She starts laughing again."
    Rhodes "What the fuck is brexit? Some kind of breakfast cereal?"
    Rhodes "You're so funny!"
    with short_pixellate
    with short_pixellate
    You "It's..."
    with short_pixellate
    with pixellate
    show white_overlay:
        alpha 0.0
        linear 5.0 alpha 0.7
    N "And then you realise you don't know what it is."
    N "Not that you can't explain it. You {i}literally{/i} don't know what it is."
    N "There's something really wrong with your head today."
    hide white_overlay
    show rhodes eb_raised e_smile m_smile at middle
    with dissolve
    You "Oh y'know, it's... {w}work stuff."
    show rhodes eb_oneup e_wide m_grin with dissolve
    Rhodes "So I'll see you tonight, {i}right?{/i}"
    You "I..."
    show rhodes eb_raised e_wide m_open with dissolve
    Rhodes "Oh! Gimme your phone a sec and I'll type in my address."
    show rhodes eb_raised e_wide m_normal with dissolve
    N "She holds out her hand and you slide your phone across the table to her."
    show rhodes eb_raised e_smile m_grin with dissolve
    Rhodes "Thanks! Wouldn't want you getting lost."  # wink
    # Rhodes "Am I gonna have to drag you there?"
    # You "N-no! {w}I'll go! I'll go."
    # Rhodes "Great!"
    # N "She holds out her hand."
    # Rhodes "Give me your phone. I'll type in my address."
    # N "You do as she says and slide it across the table to her."
    # show rhodes eb_raised e_smile m_grin:
    #     xalign 1.15 yalign 0.45 zoom 0.4
    #     ease 0.5 yalign 0.5
    # with dissolve
    # N "She taps away on it and you notice her short, black varnished nails, chipped and repainted in places."
    # TODO: bit more here?
    # N "A charm bracelet on her left wrist clinks against the table and you spy various charms on it."
    # N "A little wand that you recognise from a magical girl anime you watched as a kid, a silver pentacle with a red jewel in its centre, and a... {w}a...{w}...{w}...{w}..."

    # with short_pixellate
    # with short_pixellate
    # with long_pixellate
    # with short_pixellate

    N "When she's done she slides it back to you."
    # hide rhodes with dissolve
    # N "Did you just zone out again? Fuck."
    # N "You blink down at her outstretched hand and take your phone back."
    # N "Yeah, that's right. Her address."
    You "South London, huh... Same here."
    show rhodes eb_raised m_grin e_closed_laugh at middle with dissolve
    Rhodes "Good! You have even less of an excuse not to come!"
    You "I-I wasn't gonna make an excuse!"
    show rhodes eb_oneup e_wide m_grin with dissolve
    Rhodes "Oh sure you were!"
    show rhodes eb_raised e_wide m_normal with dissolve
    Rhodes "You seem like a serious career woman, y'know? {w}Workaholic. {w}Probably have a crisis if you stay out late."
    You "H-hey!"
    You "You make it sound like I'm super boring..."
    # N "If she thinks you're so boring, why is she even interested in you to start with?"
    show rhodes eb_sad e_wide m_open at middle_jump
    Rhodes "No! No, that's not what I meant!"
    show rhodes eb_sad e_wide_side m_normal with dissolve
    Rhodes "You're like... {w}sensible. {w}You have your life together. Unlike me."
    You "What?"
    show rhodes eb_sad e_wide m_open at middle_jump
    Rhodes "N-nevermind! I'll see you later!"
    hide rhodes with dissolve
    N "Before you can react, she's got up from the table and is dashing out of the café."

    scene black with wiperight
    $ use_alt_saybox = True

    N "You arrive at the address she gave you a little too early."
    N "Being fashionably late has never been your style after all."
    N "After she buzzes you in you take the lift up and find her flat."
    # N "Yeah, she was right."
    # N "You've never been one to arrive fashionably late - always too anxious."
    # N "If you're not at least fifteen minutes early to catch a train you'll have a panic attack."
    # N "You buzz in and Rhodes' voice crackles out of the speaker."
    # Rhodes "Yeah! Come up! {w}We're on the 11th floor!"
    # show lift_going_up
    # N "You wrench the heavy fire door open and slide in, then take the lift up."
    # N "It's an old building. Most of the places here are probably ex-council flats."
    # N "The lift seems in relatively good condition but still..."
    # N "You wouldn't want to get stuck inside it."
    # hide lift_going_up
    # scene black
    # with dissolve
    # N "When you come out the corridor is eerily quiet aside from various muffled noises coming from Rhodes' flat, presumably people moving furniture around and preparing drinks and food."
    N "You've barely knocked at the door when Rhodes opens it and drags you inside."
    scene colour_flash with wiperight
    show rhodes dark eb_raised e_smile m_smile at close with dissolve
    Rhodes "You're early! You can help us set up!"
    hide rhodes with dissolve
    N "For the next half hour she and her flatmates have you lugging packs of beer from the kitchen into the living room, filling bowls full of snacks, and unpacking all kinds of cute little finger foods."
    show crowd
    with dissolve
    N "Soon enough, other guests start to trickle in and she greets them all just as enthusiastically as she greeted you."
    N "You wonder where she fits all that energy in her little five-foot-nothing body."
    N "Before you know it, the place is genuinely packed and you can no longer see where Rhodes is."
    hide crowd with dissolve
    N "You take your drink and claim a beanbag in a corner next to the window."
    show rain
    N "It's pissing it down outside and you can hear the rain battering against the glass even over all the shouting and loud music."
    N "Suddenly the beanbag shifts."
    N "Rhodes has just sat down next to you and she leans in so she can yell in your ear."
    show rhodes body_blush eb_raised e_smile m_smile at close with dissolve
    Rhodes "This is my favourite corner!"
    N "You yell back."
    You "Yeah?"
    Rhodes "Yeah, you doin' alright?"
    You "Yeah. Bit overwhelmed."
    N "She puts her hands on your shoulders and you look up at her. She seems pretty drunk."
    Rhodes "Hey, c'mere."

    hide rhodes
    show rhodes body_blush eb_raised e_smile m_smile at zoom_face
    with dissolve

    N "She leans in close."
    N "Wow."
    N "Her eyes are so blue. {w}Like, {w}{i}really{/i} blue."

    with long_pixellate

    Rhodes "You're cute, y'know?"
    show rhodes e_closed m_normal with dissolve
    N "She leans in closer."
    N "The beanbag shifts again."
    N "Then suddenly she's kissing you."
    N "She's kissing you?"
    N "Her lips are soft and she tastes like sambuca."
    N "In that moment everything else falls away - the music, the people, the rain, the flat. Everything is gone."
    N "Everything except you and her."
    show rhodes_split at zoom_face
    hide rain
    show black_rain
    with dissolve
    N "You put your hands on her and it's as if she's made of liquid, or air, or electricity."
    show rhodes e_normal eb_normal m_normal with dissolve
    Rhodes "I really like you."
    with short_pixellate
    with short_pixellate
    N "You blink at her dumbly."
    show rhodes e_wide eb_raised m_normal with dissolve
    Rhodes "But you don't like yourself much, do you?"
    You "Wha-?"
    show rhodes e_wide eb_raised m_smile with dissolve
    # Rhodes "It's 'cause you're being held back."
    Rhodes "Gimme your hand. I'll show you."
    hide rhodes with long_dissolve
    N "You take her hand and reality melts into sludge."
    N "You fall into it."
    N "You drown in it."
    N "The lights, {w}the music, {w}her."
    N "It feels like your world is falling apart."
    N "But you're not scared by it."
    N "This is what you wanted to happen."
    N "It's what you've wanted to happen for a long time."
    with flashwhite

    # $ use_alt_saybox = False

    # N "You open your eyes again and you're back in high school."
    # Rhodes "These are moments you regret. I can help you change them."
    # You "Regret?"

    show rhodes_transparent at zoom_face with long_dissolve
    Rhodes "You wish you were different, don't you?"
    Rhodes "Well... not {i}really{/i} different."
    Rhodes "More like your true self."

    menu:
        Rhodes "You know what I mean?"
        "Kinda":
            pass

    Rhodes "But you've been held back."
    Rhodes "There's nothing wrong with how you want to be."

    menu:
        Rhodes "It's the world that's wrong."
        "The world?":
            pass

    Rhodes "Yeah. You know what I mean."
    Rhodes "Think back."
    Rhodes "Remember what you felt."

    menu:
        Rhodes "23 June 2016"
        "I want people to accept me":
            pass

    Rhodes "Remember."

    menu:
        Rhodes "16 March 2020"
        "I don't want to be alone":
            pass

    Rhodes "Remember."
    Rhodes "Now change the world."

    $ use_alt_saybox = False

    hide rhodes_split
    hide black_rain
    hide rhodes_transparent

    scene colour_flash_light with flashwhite

    show explode
    show explode2
    with dissolve

    N "Reality explodes around you, fragments of it combining and recombining in impossible ways."
    N "Along with it, you can feel your memories recombining."
    N "Lining up. {w}Smoothing out. {w}Making sense."
    N "You remember."

    scene white
    with long_dissolve
    with Pause(2.0)

    show rhodes_empty glitch at middle_pan_up

    Rhodes "Remember."
    Rhodes "You know who I am."
    You "I do?"
    You "No..."
    hide rhodes_empty glitch
    show rhodes_empty glitch at close_right
    with long_dissolve
    You "Of course I do."
    You "I've known all along."
    hide rhodes_empty
    show rhodes_flip eb_raised e_wide m_open at close_right
    with dissolve
    You "Rhodes!"
    You "I remember!"
    show rhodes_flip e_closed_laugh m_laugh at close_right
    with dissolve
    You "This is me!"
    scene black with Dissolve(2.0)
    with Pause(1.0)

    return
